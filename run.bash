#!/usr/bin/env bash

set -e

go build marsdrop.go vec.go rk4.go

time ./marsdrop

gnuplot plot.gp

