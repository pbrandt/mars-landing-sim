import math

from require import require

logger = require('../util/logger.py')
sim = require('../sim/index.py')
interpolate = require('../util/interpolate.py')

#
# LAME ASS PARACHUTE SIM
# but at least it's on mars
# based on the marsDROP concept
# https://digitalcommons.usu.edu/cgi/viewcontent.cgi?article=3237&context=smallsat
#
MARS_GRAVITY_MS2 = 3.72076
MARS_ATM_SPECIFIC_HEAT = 1.29 # http://www.aerospaceweb.org/question/atmosphere/q0249.shtml
MARS_ATM_GAS_CONSTANT = 191.8 # J/kg/K

class Properties:
    velocity_kms = 0
    airspeed_kms = 0
    speed_of_sound_kms = 0
    mach_number = 0
    temperature_k = 0
    pressure_kpa = 0
    density_kgm3 = 0
    cd = 0
    dynamic_pressure_kpa = 0
    drag_kms2 = 0
    drag_g = 0
    dragx_kms2 = 0
    dragy_kms2 = 0

def calculate_properties(state):
    # alias state properties
    met_s = state[0] # mission elapsed time in seconds
    x_km = state[1]
    y_km = state[2]
    altitude_km = y_km
    vx_kms = state[3]
    vy_kms = state[4]
    drag_area_m2 = state[5]
    mass_kg = state[6]

    velocity_kms = math.sqrt(vx_kms * vx_kms + vy_kms * vy_kms)
    airspeed_kms = velocity_kms # assuming no wind model

    # Temperature in kelvin, pressure in KPa, density in kg/m3
    # https://www.grc.nasa.gov/WWW/K-12/airplane/atmosmrm.html
    # https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20010056680.pdf
    if altitude_km > 44.9:
        temperature_k = 150
    elif altitude_km > 7:
        temperature_k = 249.75 - 2.22 * altitude_km
    else:
        temperature_k = 242.15 - 0.998 * altitude_km
    
    if altitude_km > 70:
        density_kgkm3 = math.pow(10, 2 + 2 / 30 * (100 - altitude_km))
        density_kgm3 = density_kgkm3 / 1e9
    else:
        density_kgm3 = math.pow(10, -3 + 1 / 26 * (30 - altitude_km))

    pressure_kpa = 0.699 + math.exp(-0.09 * altitude_km)
    # density_kgm3 = pressure_kpa / (0.1921 * temperature_k)

    # Drag Coefficient (this is hard)
    # https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20080034648.pdf
    # depends on the aerodynamic regime, so you need mach number
    # not calculating knudsen number, just assuming free molecular and transitional flow above a certain altitude
    speed_of_sound_kms = math.sqrt(MARS_ATM_SPECIFIC_HEAT * MARS_ATM_GAS_CONSTANT * temperature_k) / 1000
    mach_number = airspeed_kms / speed_of_sound_kms

    if altitude_km > 60:
        # Free molecular and transitional flow
        cd = 1.7 + (1.85 - 1.7) / (100 - 60) * (altitude_km - 60)
    elif mach_number > 5:
        # hypersonic
        # table is x: km/s, y: Cd, figure 9.a of that pdf in the references section
        table = [
            [0, 1.6],
            [2, 1.625],
            [4, 1.725],
            [5.6, 1.725],
            [8, 1.7]
        ]

        cd = interpolate(table, velocity_kms)
    else:
        # Supersonic and subsonic
        # table is x: mach, y: Cd
        table = [
            [0, 0.5],
            [0.4, 1.15],
            [0.6, 1.1],
            [0.8, 1.175],
            [1, 1.3],
            [1.2, 1.425],
            [1.5, 1.55],
            [2, 1.575],
            [5.1, 1.6]
        ]
        cd = interpolate(table, mach_number)
    
    dynamic_pressure_kpa = 0.5 * density_kgm3 * airspeed_kms * airspeed_kms * 1000

    # assume no lift, angle of attak is zero
    drag_kms2 = dynamic_pressure_kpa * drag_area_m2 * cd / mass_kg
    drag_g = drag_kms2 / MARS_GRAVITY_MS2 * 1000
    dragx_kms2 = -1 * drag_kms2 * vx_kms / velocity_kms
    dragy_kms2 = -1 * drag_kms2 * vy_kms / velocity_kms


    values = Properties()
    values.velocity_kms = velocity_kms
    values.airspeed_kms = airspeed_kms
    values.speed_of_sound_kms = speed_of_sound_kms
    values.mach_number = mach_number
    values.temperature_k = temperature_k
    values.pressure_kpa = pressure_kpa
    values.density_kgm3 = density_kgm3
    values.cd = cd
    values.dynamic_pressure_kpa = dynamic_pressure_kpa
    values.drag_kms2 = drag_kms2
    values.drag_g = drag_kms2
    values.dragx_kms2 = dragx_kms2
    values.dragy_kms2 = dragy_kms2
        
    return values

entry_veloctiy = 7 # km/s
flight_path_angle = -13 # degrees

vx = entry_veloctiy * math.cos(flight_path_angle * math.pi/180)
vy = entry_veloctiy * math.sin(flight_path_angle * math.pi/180)

state = [
    0.0, # t
    0.0, # x km
    100.0, # y km
    vx, # vx km/s
    vy, # vy km/z
    # 2.65 * 2.65 * math.pi / 4, # pathfinder
    # 585 # kg pathfinder
    0.3 * 0.3 * math.pi / 4, # drag area m2
    3.0, # mass kg
]

def derivative_function(state):
    vx_kms = state[3]
    vy_kms = state[4]
    mass_kg = state[6]
    area_m2 = state[5]
    altitude_km = state[2]

    derived = calculate_properties(state)

    # import pdb; pdb.set_trace()

    return [
        1,
        vx_kms,
        vy_kms,
        derived.dragx_kms2,
        -1 * MARS_GRAVITY_MS2 / 1000 + derived.dragy_kms2,
        0,
        0
    ]

def log(state):
    derived = calculate_properties(state)
    data = state + [
        derived.airspeed_kms,
        derived.speed_of_sound_kms,
        derived.mach_number,
        derived.temperature_k,
        derived.pressure_kpa,
        derived.density_kgm3,
        derived.cd,
        derived.dynamic_pressure_kpa,
        derived.drag_kms2,
        derived.drag_g,
        derived.dragx_kms2,
        derived.dragy_kms2
    ]



    logger.log(f't: {round(data[0], 1)} x: {data[1]} km, y: {data[2]} km, vx: {round(data[3], 2)} km/s, vy: {round(data[4], 2)}')
    logger.log(f'gs: {derived.drag_g}, mach: {derived.mach_number}, vinf: {derived.airspeed_kms}, t_k: {derived.temperature_k}, density_kgm3: {derived.density_kgm3}')
    logger.log(data[len(state):])
    input("press enter to continue")
    return data

# Initialize the data log
data_log = [log(state)]

def main():
    """ WE"VE GOT A SIM """
    def end_condition_wrapper(fn):
        """ adds a safety check for y < 0 on top of all other checks """
        def safe_fn(state):
            return state[2] < 0 or fn(state)
        
        return safe_fn
    

    def end_entry_phase(state):
        """ backshell separation occurs at an altitude of 6.5 km """
        return state[2] < 6.5
    
    def main_deploy_phase(state):
        """ main chutes deploy at speed of 0.2 km/s """
        vx = state[3]
        vy = state[4]
        vmag = math.sqrt(vx * vx  +  vy * vy)
        return vmag < 0.2

    # starting from the entry interface, propagate until backshell separation
    logger.log('STARTING SIM')
    data = sim.run(state, derivative_function, end_condition_wrapper(end_entry_phase), 0.1, log, 0.5)
    for d in data:
        data_log.append(d)


    logger.log('BACKSHELL SEPARATION')


    # plot results or something
    logger.log('DONE')


if __name__ == '__main__':
    main()
