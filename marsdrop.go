package main;

import (
	"fmt"
	"os"
	"bufio"
	"math"
)

// define some constants
const MARS_GRAVITY_MS2 = 3.72076
const MARS_ATM_SPECIFIC_HEAT = 1.29 // http://www.aerospaceweb.org/question/atmosphere/q0249.shtml
const MARS_ATM_GAS_CONSTANT = 191.8 // J/kg/K
var SPACECRAFT_DRAG_AREA = 0.3 * 0.3 * 3.14159 / 4 // drag area in m2
var SPACECRAFT_MASS = 3.0 // kg

// x is km/s, y is CD, figure 9.a of that pdf in the references section
var HYPERSONIC_DRAG_TABLE = [5][2]float64{
	{0, 1.6},
	{2, 1.625},
	{4, 1.1725},
	{5.6, 1.725},
	{8, 1.7}}

// x is mach, y is CD
var SUPERSONIC_SUBSONIC_DRAG_TABLE = [9][2]float64{
	{0, 0.5},
	{0.4, 1.15},
	{0.6, 1.1},
	{0.8, 1.175},
	{1, 1.3},
	{1.2, 1.425},
	{1.5, 1.55},
	{2, 1.575},
	{5.1, 1.6}}

// TODO make calcaulate properties function i guess that's a good idea.
type Props struct {
	altitude_km, velocity_kms, r_km,
	temperature_k, density_kgm3, pressure_kpa,
	speed_of_sound_kms, mach_number, dynamic_pressure_kpa,
	cd float64
	drag [2]float64
}

// calculates the properties that are not part of the integrated state vector
func calculateProps(x []float64, t float64) (p Props) {
	// calculate derived properties
	p.altitude_km = x[1]
	p.r_km = Norm(x[0:2])
	p.velocity_kms = math.Sqrt(x[2] * x[2] + x[3] * x[3])

	//-------------------------------------------------------
	// ATMOSPHERE
    // Temperature in kelvin, pressure in KPa, density in kg/m3
    // https://www.grc.nasa.gov/WWW/K-12/airplane/atmosmrm.html
	// https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20010056680.pdf
	if p.altitude_km > 44.9 {
		p.temperature_k = 150
	} else if p.altitude_km > 7 {
		p.temperature_k = 249.75 - 2.22 * p.altitude_km
	} else {
		p.temperature_k = 242.15 - 0.998 * p.altitude_km
	}

	p.pressure_kpa = 0.699 * math.Exp(-0.09 * p.altitude_km)

	p.density_kgm3 = p.pressure_kpa / (0.1921 * p.temperature_k)


	// if p.altitude_km > 70 {
	// 	p.density_kgm3 = math.Pow(10, 2 + 2 / 30 * (100 - p.altitude_km)) / 1e9
	// } else {
	// 	p.density_kgm3 = math.Pow(10, -3 + 1 / 26 * (30 - p.altitude_km))
	// }


	//-------------------------------------------------------
	// DRAG
    // https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20080034648.pdf
	// depends on the aerodynamic regime, so you need mach number
    // not calculating knudsen number, just assuming free molecular and transitional flow above a certain altitude
    p.speed_of_sound_kms = math.Sqrt(MARS_ATM_SPECIFIC_HEAT * MARS_ATM_GAS_CONSTANT * p.temperature_k) / 1000
	p.mach_number = p.velocity_kms / p.speed_of_sound_kms // assumes no wind model
	p.dynamic_pressure_kpa = 0.5 * p.density_kgm3 * p.velocity_kms * p.velocity_kms * 1000

	if p.altitude_km > 60 {
		// Free molecular and transitional flow
		p.cd = 1.7 + (1.85 - 1.7) / (100 - 60) * (p.altitude_km - 60)
	} else if p.mach_number > 5 {
		p.cd = Interpolate(HYPERSONIC_DRAG_TABLE[:][:], p.velocity_kms)
	} else {
		p.cd = Interpolate(SUPERSONIC_SUBSONIC_DRAG_TABLE[:][:], p.mach_number)
	}

	copy(p.drag[:], Mul(-1 * p.dynamic_pressure_kpa * p.cd * SPACECRAFT_DRAG_AREA / SPACECRAFT_MASS / p.velocity_kms, x[2:4]))

	return p
}

// calculate derivatives
func fx(x []float64, t float64) []float64 {
	var p Props = calculateProps(x, t)

	deriv := make([]float64, len(x), len(x))

	// position derivative is the velocity
	copy(deriv[0:2], x[2:4])

	// velocity derivative is the acceleration due to gravity and drag
	copy(deriv[2:4], p.drag[:])

	// gravity, assumed constant for now
	gravity := 3.72076
	deriv[3] = deriv[3] - gravity

	return deriv
}

func log(x []float64, t float64, w *bufio.Writer) {
	var p Props = calculateProps(x, t)
	w.WriteString(fmt.Sprintf("%-1g, %-1g, %-1g, %-1g, %-1g, %-1g, %-1g, %-1g, %-1g, %-1g, %-1g, %-1g, %-1g, %-1g\n",
		t,
		x[0], x[1], p.r_km,
		x[2], x[3], p.velocity_kms,
		p.speed_of_sound_kms, p.mach_number,
		p.temperature_k, p.pressure_kpa, p.density_kgm3,
		p.cd, p.dynamic_pressure_kpa))
}

func main() {
	// set up the entry conditions
	entry_velocity := 7.0 // km/s
	flight_path_angle := -13.0 // degrees
	vx := entry_velocity * math.Cos(flight_path_angle * 3.14159 / 180.0)
	vy := entry_velocity * math.Sin(flight_path_angle * 3.14159 / 180.0)
	x0 := [5]float64{
		0.0,
		100.0,
		vx,
		vy,
		SPACECRAFT_MASS}
	
	x := x0[:]

	var t float64 = 0
	var dt float64 = 0.01

	// create log file
	f, err := os.Create("./data/log.csv")
	if err != nil {
		panic(err)
	}
	w := bufio.NewWriter(f)
	w.WriteString("t {s}, x {km}, y {km}, r {km}, vx {km/s}, vy {km/s}, v {km/s}, speed of sound {km/s}, mach {--}, temp {K}, pressure {kPa}, density {kg/m3}, cd {--}, q {kPa}\n")

	fmt.Println("starting simulation")

	fmt.Println("entry phase")
	fmt.Println(x[1] < 6.5)
	for ; x[1] > 6.5; {
		if int(t*100) % 10 == 0 {
			log(x, t, w)
		}

		if t >= 1 && int(t) % 60 == 0 {
			fmt.Printf("minute %-1g, x is %-1g\n", t / 60, x[0])
		}

		x = Integrate(x, t, fx, dt)
		t = t + dt
	}

	// log final point
	log(x, t, w)
	fmt.Println(x)

	fmt.Println("done")
	w.Flush()
}
