set grid
set key autotitle columnhead
set datafile separator ","
set terminal pngcairo dashed size 800,600 enhanced background rgb "white" linewidth 1 font "Public Sans,14"


# Position plot x vs y
set title "MarsDrop trajectory"
set xlabel "x (km)"
set ylabel "y (km)"
set output "data/trajectory.png"
plot "data/log.csv" using 2:3 with lines notitle

# Altitude plot
set title "MarsDrop altitude"
set xlabel "t (s)"
set ylabel "altitude (km)"
set output "data/altitude.png"
plot "data/log.csv" using 1:3 with lines notitle

# Velocity plot
set title "MarsDrop velocity"
set xlabel "t (s)"
set ylabel "velocity (km/s)"
set output "data/velocity.png"
plot "data/log.csv" using 1:7 with lines notitle

# Mach
set title "MarsDrop mach number"
set xlabel "t (s)"
set ylabel "mach"
set output "data/mach.png"
plot "data/log.csv" using 1:9 with lines notitle

# atmosphere properties
set title "Local atmospheric temperature"
set xlabel "t (s)"
set ylabel "temperature (K)"
set output "data/temperature.png"
plot "data/log.csv" using 1:10 with lines notitle

set title "Local atmospheric temperature vs altitude"
set ylabel "altitude (km)"
set xlabel "temperature (K)"
set xrange [100:300]
set output "data/temperature-altitude.png"
plot "data/log.csv" using 10:3 with lines notitle
unset xrange

set title "Local atmospheric pressure"
set xlabel "t (s)"
set ylabel "pressure (kPa)"
set output "data/pressure.png"
plot "data/log.csv" using 1:11 with lines notitle

set title "Local atmospheric pressure vs altitude"
set ylabel "altitude (km)"
set xlabel "pressure (kPa)"
set output "data/pressure-altitude.png"
set logscale x
plot "data/log.csv" using 11:3 with lines notitle
unset logscale x

set title "Local atmospheric density"
set xlabel "t (s)"
set ylabel "density (kg/m3)"
set output "data/density.png"
plot "data/log.csv" using 1:12 with lines notitle

set title "Local atmospheric density vs altitude"
set ylabel "altitude (km)"
set xlabel "density (kg/m3)"
set output "data/density-altitude.png"
set logscale x
plot "data/log.csv" using 12:3 with lines notitle
unset logscale x

# drag
set title "MarsDrop drag coefficient"
set xlabel "t (s)"
set ylabel "Cd"
set output "data/cd.png"
plot "data/log.csv" using 1:13 with lines notitle

set title "MarsDrop dynamic pressure"
set xlabel "t (s)"
set ylabel "q (kPa)"
set output "data/q.png"
plot "data/log.csv" using 1:14 with lines notitle
