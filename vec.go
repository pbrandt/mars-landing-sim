package main

import (
	"math"
)

func Add(v1 []float64, v2 []float64) []float64 {
	v_out := make([]float64, len(v1))
	for i, v := range v1 {
		v_out[i] = v + v2[i]
	}
	return v_out
}

func Mul(c float64, v []float64) []float64 {
	v_out := make([]float64, len(v))
	for i, x := range v {
		v_out[i] = c * x
	}
	return v_out
}

func Norm(v []float64) float64 {
	norm := 0.0
	for _, x := range v {
		norm = norm + x * x
	}
	return math.Sqrt(norm)
}

func Unit(v []float64) []float64 {
	norm := Norm(v)
	return Mul(1 / norm, v)
}

func Concatenate(vecs ...[]float64) []float64 {
	total_length := 0
	for _, vec := range vecs {
		total_length = total_length + len(vec)
	}

	out_vec := make([]float64, total_length)
	i := 0
	for _, vec := range vecs {
		for _, x := range vec {
			out_vec[i] = x
			i = i + 1
		}
	}

	return out_vec
}

func Interpolate(table [][2]float64, x float64) (y float64) {
	// logger.debug(f'interpolate x = {x}')
    low := table[0]
    high := table[len(table) - 1]
    
    if x < low[0] {
        // logger.log(f'Warning: interpolating out of bounds, giving lowest bound. was {x}')
		return low[1]
	} else if x > high[0] {
        // logger.log(f'Warning: interpolating out of bounds, giving highest bound. was {x}')
        return high[1]
	}

	for i := 1; i < len(table); i++ {
		if table[i][0] > x {
			low := table[i-1]
			high := table[i]
			return (high[1] - low[1]) / (high[0] - low[0]) * (x - low[0]) + low[1]
		}
	}
	// should never get here
	return high[1]
}