package main

type derivative func(x []float64, t float64) []float64

func Integrate(x0 []float64, t0 float64, f derivative, dt float64) []float64 {
	var k1, k2, k3, k4 []float64
	k1 = f(x0, t0)
	k2 = f(Add(x0, Mul(dt/2, k1)), t0 + dt/2)
	k3 = f(Add(x0, Mul(dt/2, k2)), t0 + dt/2)
	k4 = f(Add(x0, Mul(dt, k3)), t0 + dt)

	x_out := Add(x0,   Mul(dt/6, k1))
	x_out = Add(x_out, Mul(dt/3, k2))
	x_out = Add(x_out, Mul(dt/3, k3))
	x_out = Add(x_out, Mul(dt/6, k4))

	return x_out
}