# MarsDrop small capsule sim

![mars drop conops](./mars-drop-conops.png)


## reading / resources

- https://digitalcommons.usu.edu/cgi/viewcontent.cgi?article=3237&context=smallsat
- http://www.aerospaceweb.org/question/atmosphere/q0249.shtml
- https://www.grc.nasa.gov/WWW/K-12/airplane/atmosmrm.html
- https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20010056680.pdf
- https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20080034648.pdf
